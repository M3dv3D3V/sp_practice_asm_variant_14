#include <iostream>

using namespace std;

int main(){
    int result_before = 0;
    int result_after =  0;
    unsigned char array[] = {10, 20, 30, 40, 50};
    for (int i = 0; i < sizeof(array); i++)
    {
        result_before += array[i];
        unsigned char after_one_bit = array[i] | 0xf;
        result_after += after_one_bit;
    }
    cout << "Result before setup 4 bits to 1 = " << result_before << endl;
    cout << "Result after setup 4 bits to 1 = " << result_after << endl;
    return 0;
}