#!/bin/bash

nasm -g -F dwarf -f elf64 $1.asm
gcc -m64 -o output $1.o -no-pie -O3